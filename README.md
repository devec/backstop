# backstopjs & backstop-crawl

## Installation

 git clone https://gitlab.com/devec/backstop

 docker build -t backstop .

## Anwenden

 cd path/to/projectdir

 mkdir backstop-tests

 cd backstop-tests

 docker run --rm -v $(pwd):/src backstop backstop-crawl http://hostname

Durch diesen Befehl wird eine backstop.json erzeugt. backstop-crawl geht dabei alle Seiten der Website durch und legt Einträge dafür an. Die backstop.js muss unter Umständen danach noch konfiguriert werden z.B. für dynamischer Content. (siehe Wiki oder https://github.com/garris/BackstopJS)

 docker run --rm -v $(pwd):/src backstop backstop reference

 docker run --rm -v $(pwd):/src backstop backstop test

Mit dem Befehl reference wird eine referenze angelegt. (später können hiermit auch neue referenzen angelegt werden) Mit dem Befehl test werden die Bitmaps angelegt.

 docker run --rm -v $(pwd):/src backstop backstop test

Verändert man nun etwas so kann mit einem  weiterem test dies zur reference verglichen werden.



